var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(skill_id, callback) {
    var query = 'SELECT skill_name,description from' +
        ' skill where skill_id = ?'
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE skill
    var query = 'INSERT INTO skill (skill_name,description) VALUES (?,?)';
    var queryData = [params.skill_name,params.description];
    console.log(queryData);
    connection.query(query, queryData, function(err, result) {
        /*
         // THEN USE THE skill_ID RETURNED AS insertId AND THE SELECTED skill_IDs INTO skill_skill
         var skill_id = result.insertId;

         // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
         var query = 'INSERT INTO skill_skill (skill_id, skill_id) VALUES (?)';

         // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
         var skillskillData = [];
         for(var i=0; i < params.skill_id.length; i++) {
         skillskillData.push([skill_id, params.skill_id[i]]);
         }

         // NOTE THE EXTRA [] AROUND skillskillData
         connection.query(query, [skillskillData], function(err, result){
         callback(err, result);

         });
         */
    });

};

exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE skill SET skill_name = ?, description = ? WHERE skill_id = ?';
    var queryData = [params.skill_name, params.description,params.skill_id];
    console.log("PARAMS: ",params);
    console.log( queryData);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);

    });
};

exports.edit = function(skill_id, callback) {
    console.log("calling getinfo");
    var query = 'CALL skill_getinfo(?)';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
